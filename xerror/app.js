const request = require('request');
const express = require('express');
const bodyParser = require("body-parser");
const path = require('path');

const fs = require('fs');
const multer  = require('multer');
const upload = multer({ dest: 'uploads/' })
const app = express();

const mustache = require('mustache-express');
const viewsDir = path.join(__dirname,"/views");
app.engine('mst', mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use(express.static(__dirname + '/public'));

const urlencodedParser = bodyParser.urlencoded({extended: false});

//Endpoints
app.get("/", urlencodedParser, (req, res) => {
    res.sendFile(path.join(__dirname + '/views/index.html'));
});

app.get("/compile", (req, res) => {
    // var response = "response";
    // var error = "error";
    //res.render('result', {result: error == null ? response : error});
    res.render('compile');

});

app.post("/compile", upload.single('file'), (req, res) => {
    if (!req.body) {
        //console.log('error line 36');
        return res.sendStatus(400);
    }
    console.log(req.body);        
    if (req.file == null)
        res.redirect('../compile');
    var contents = fs.readFileSync(req.file.path, 'utf8');
    console.log(contents);
    var program = {
        script: `${contents}`,
        language: `${req.body.language}`,
        versionIndex: "0",
        clientId: "5fe3910c85acdff78216e0893c0bf356",
        clientSecret: "4ae774c9b842aa0c876a0274de3d977d12eead354f8fa8c42d21936b86bb532f"
    };
    request({
        url: 'https://api.jdoodle.com/v1/execute', //'https://api.jdoodle.com/v1/credit-spent', --> to check left amount of requests
        method: "POST",
        json: program
    },
    (error, response, body) => { 
        res.render('result', body.memory == null ? { response : body.output, text : "Oops, looks like error:", text_style : "text-danger"}
                                                    : { response : body.output, text : "Here is result:", text_style: "text-success"});
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        console.log('body:', body);
        // console.log('--------------');
        // console.log('body:', body.output);
        // console.log('memory:', body.memory);
    });
});

app.listen(3000, () => {
    console.log('Server is listening on port 3000!');
});